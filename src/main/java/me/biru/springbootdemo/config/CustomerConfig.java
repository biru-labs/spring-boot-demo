package me.biru.springbootdemo.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "demo.customer")
public class CustomerConfig {
	/**
	 * Label used in customer's names prefix
	 */
	private String prefixLabel = "Dear ";
}
