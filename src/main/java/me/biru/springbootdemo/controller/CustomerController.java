package me.biru.springbootdemo.controller;

import lombok.RequiredArgsConstructor;
import me.biru.springbootdemo.config.CustomerConfig;
import me.biru.springbootdemo.dto.Customer;
import me.biru.springbootdemo.exception.EntityNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@RestController
@RequestMapping(path = "customers")
@RequiredArgsConstructor
public class CustomerController {
	private final CustomerConfig customerConfig;

	private List<Customer> customers = new ArrayList<>(asList(
			new Customer(1, "John"),
			new Customer(2, "James"),
			new Customer(3, "Henri"))
	);

	@GetMapping
	public List<Customer> getCustomers(@RequestParam(required = false) boolean prefix) {
		if (prefix) {
			return customers.stream()
					.map(c -> c.toBuilder().name(customerConfig.getPrefixLabel() + c.getName()).build())
					.collect(Collectors.toList());
		} else {
			return customers;
		}
	}

	@GetMapping(path = "/{id}")
	public Customer getCustomersById(@PathVariable int id) {
		return customers.stream()
				.filter(c -> c.getId() == id)
				.findFirst()
				.orElseThrow(() -> new EntityNotFoundException("Customer " + id + " not found"));
	}


	@PostMapping
	public ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		this.customers.add(customer);
		return new ResponseEntity<>(customer, HttpStatus.CREATED);
	}
}
