package me.biru.springbootdemo.controller;

import me.biru.springbootdemo.exception.EntityNotFoundException;
import me.biru.springbootdemo.exception.ErrorEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.UUID;

@ControllerAdvice
public class GlobalExceptionHandler {
	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<ErrorEntity> handleEntityNotFound(EntityNotFoundException ex) {
		return new ResponseEntity<>(new ErrorEntity(UUID.randomUUID().toString(), ex.getMessage()), HttpStatus.NOT_FOUND);
	}
}
