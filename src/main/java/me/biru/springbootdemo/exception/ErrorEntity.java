package me.biru.springbootdemo.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorEntity {
	private String traceId;
	private String message;
}
