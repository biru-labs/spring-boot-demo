# Spring Boot Demo

## 1

- Basic set-up (entry point, yml, controller)
- @RequestBody
- lombok

## 2

- devtools with Ctrl-F9 compile 
- [Actuators](https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-endpoints.html)
	- yml config with autocomplete
- [Swagger](http://localhost:9042/swagger-ui.html)

## 3
		
- getCustomerById + GlobalExceptionHandler + custom Error object

## 4

- Custom configuration
	- entry point scan
	- config class + yml
	- autocomplete, documentation
	- builder pattern
	- injection in controller
- @RequestParam optional